# Information / Информация

Добавление различных типов ссылок в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_URL`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_URL' );
```

## Syntax / Синтаксис

```html
{{#url: [TYPE]|[CONTENT]|[TITLE]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
